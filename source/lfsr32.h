#ifndef lfsr32_h
#define lfsr32_h

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/** \brief Simulate n clock cycles of a 32-bit Linear Feedback Shift Register

    The contents of the LFSR are manipulated in-place and returned, making this
    function extremely efficient.

    Useful Links:
    - https://en.wikipedia.org/wiki/Linear-feedback_shift_register
    - https://www.eetimes.com/tutorial-linear-feedback-shift-registers-lfsrs-part-1
    - https://users.ece.cmu.edu/~koopman/lfsr/index.html
    - https://www.maximintegrated.com/en/design/technical-documents/app-notes/4/4400.html
    
    \param value LFSR contents
    \param mask Binary mask that is XORed with value after every shift

    \return Updated LFSR value
*/
extern uint32_t lfsr32(uint32_t value, uint32_t mask);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* lfsr32_h */
