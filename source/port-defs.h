#ifndef PORT_DEFS_H
#define PORT_DEFS_H

// Generic Pin Names
#define _P(x,n) P##x##n
#define P(x,n) _P(x,n)

// Port Data Registers
#define _PORT(x) PORT##x
#define PORT(x) _PORT(x)

// Data Direction Registers
#define _DDR(x) DDR##x
#define DDR(x) _DDR(x)
#define DD(x,n) DD##x##n

// Input Pin Registers
#define _PIN(x) PIN##x
#define PIN(x) _PIN(x)

// Pull-up Enable Registers
#if defined PUEA || defined PUEB
#define _PUE(x) PUE##x
#define PUE(x) _PUE(x)
#endif /* PUEA || PUEB */

// Pin Change Mask Registers
#ifndef PCMSK
#define _PCMSK(n) PCMSK##n
#define PCMSK(n) _PCMSK(n)
#endif /* PCMSK */

// Pin Change Interrupt Bits
#define _PCINT(n) PCINT##n
#define PCINT(n) _PCINT(n)

#endif
