#ifndef TIMER_DEFS_H
#define TIMER_DEFS_H

/* Timer/Counter Control Registers
   TCCRnA [ COMnA1 | COMnA0 | COMnB1 | COMnB0 | ------ | ------ |  WGMn1 |  WGMn0 ]
   TCCRnB [  ICNCn |  ICESn | ------ |  WGMn3 |  WGMn2 |   CSn2 |   CSn1 |   CSn0 ]
   TCCRnC [  FOCnA |  FOCnB | ------ | ------ | ------ | ------ | ------ | ------ ]
*/
#define _TCCR(n,x) TCCR##n##x
#define TCCR(n,x) _TCCR(n,x)

// TCCRnA/B/C configuration bits
#define _COM(n,x,i) COM##n##x##i
#define COM(n,x,i) _COM(n,x,i)
#define _WGM(n,i) WGM##n##i
#define WGM(n,i) _WGM(n,i)
#define _CS(n,i) CS##n##i
#define CS(n,i) _CS(n,i)
#define _FOC(n,x) FOC##n##x
#define FOC(n,x) _FOC(n,x)

// Timer/Counter Registers
#define _TCNT(n) TCNT##n
#define TCNT(n) _TCNT(n)
// High bits
#define _TCNT_H(n) TCNT##n##H
#define TCNT_H(n) _TCNT_H(n)
// Low bits
#define _TCNT_L(n) TCNT##n##L
#define TCNT_L(n) _TCNT_L(n)

// Output Compare Register
#define _OCR(n) OCR##n
#define OCR(n) _OCR(n)
// High bits
#define _OCR_H(n,x) OCR##n##x##H
#define OCR_H(n,x) _OCR_H(n,x)
// Low bits
#define _OCR_L(n,x) OCR##n##x##L
#define OCR_L(n,x) _OCR_L(n,x)

// Input Capture Register
#define _ICR(n) ICR##n
#define ICR(n) _ICR(n)
// High bits
#define _ICR_H(n) ICR##n##H
#define ICR_H(n) _ICR_H(n)
// Low bits
#define _ICR_L(n) ICR##n##L
#define ICR_L(n) _ICR_L(n)

/* Timer/Counter Interrupt Mask Register
   TIMSKn [ ------ | ------ |  ICIE0 | ------ | ------ | OCIEnB | OCIEnA |  TOIEn ]
*/
#define _TIMSK(n) TIMSK##n
#define TIMSK(n) _TIMSK(n)
// Configuration bits
#define _ICIE(n) ICIE##n
#define ICIE(n) _ICIE(n)
#define _OCIE(n,x) OCIE##n##x
#define OCIE(n,x) _OCIE(n,x)
#define _TOIE(n) TOIE##n
#define TOIE(n) _TOIE(n)

/* Timer/Counter Interrupt Flag Register
   TIFRn  [ ------ | ------ |   ICFn | ------ | ------ |  OCFnB |  OCFnA |   TOVn ]
*/
#define _TIFR(n) TIFR##n
#define TIFR(n) _TIFR(n)
// Configuration bits
#define _ICF(n) ICF##n
#define ICF(n) _ICF(n)
#define _OCF(n,x) OCF##n##x
#define OCF(n,x) _OCF(n,x)
#define _TOV(n) TOV##n
#define TOV(n) _TOV(n)

#endif /* TIMER_DEFS_H */
